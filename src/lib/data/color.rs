use std::fmt::Debug;

use crate::conversion::color_mode::ToRgb8;

pub trait Color: Debug + ToRgb8 {}

mod rgb;
pub use rgb::*;

mod rgb8;
pub use rgb8::*;

mod hsv;
pub use hsv::*;

mod hsl;
pub use hsl::*;
