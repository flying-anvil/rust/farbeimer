use crate::conversion::color_mode::{ToRgb, ToRgb8, ToHsv};

use super::{RGB, Color};

/// Red, Green, Blue
/// In range of 0..=255
#[derive(Debug, Clone, Copy)]
pub struct RGB8 {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

impl RGB8 {
    pub fn new(red: u8, green: u8, blue: u8) -> Self {
        Self { red, green, blue }
    }
}

impl Color for RGB8 {}

impl ToRgb for RGB8 {
    fn to_rgb(&self) -> RGB {
        RGB::new(
            self.red as f32 * 255.0,
            self.green as f32 * 255.0,
            self.blue as f32 * 255.0,
        )
    }
}

impl ToRgb8 for RGB8 {
    fn to_rgb8(&self) -> RGB8 {
        self.clone()
    }
}

impl ToHsv for RGB8 {
    /// It's a chain conversion, as RGB8::to_rgb only introduces a cannel-wise multiplication, which is still necessary.
    fn to_hsv(&self) -> super::HSV {
        self.to_rgb().to_hsv()
    }
}
