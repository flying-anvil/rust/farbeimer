use crate::conversion::color_mode::ToRgb8;

use super::{Color, RGB8};

/// Hue, Saturation, Lightness
/// In range of {
///   hue: 0..=360
///   saturation: 0..=1
///   lightness: 0..=1
/// }
#[derive(Debug, Clone)]
pub struct HSL {
    pub hue: f32,
    pub saturation: f32,
    pub lightness: f32,
}

impl HSL {
    pub fn new(hue: f32, saturation: f32, lightness: f32) -> Self {
        Self {
            hue,
            saturation,
            lightness,
        }
    }
}

impl Color for HSL {}

impl ToRgb8 for HSL {
    fn to_rgb8(&self) -> RGB8 {
        todo!()
    }
}
