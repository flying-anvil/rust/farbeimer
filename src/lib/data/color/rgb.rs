use crate::conversion::color_mode::{ToRgb, ToRgb8, ToHsv};

use super::{RGB8, HSV, Color};

/// Red, Green, Blue
/// In range of 0..=1
#[derive(Debug, Clone)]
pub struct RGB {
    pub red: f32,
    pub green: f32,
    pub blue: f32,
}

impl RGB {
    pub fn new(red: f32, green: f32, blue: f32) -> Self {
        Self { red, green, blue }
    }
}

impl Color for RGB {}

impl ToRgb for RGB {
    fn to_rgb(&self) -> RGB {
        self.clone()
    }
}

impl ToRgb8 for RGB {
    fn to_rgb8(&self) -> RGB8 {
        RGB8::new(
            (self.red * 255.0).round() as u8,
            (self.green * 255.0).round() as u8,
            (self.blue * 255.0).round() as u8,
        )
    }
}

impl ToHsv for RGB {
    fn to_hsv(&self) -> HSV {
        let max = self.red.max(self.green).max(self.blue);
        let min = self.red.min(self.green).min(self.blue);

        let diff = max - min;
        let saturation = if max == 0.0 {0.0} else {diff / max};

        let hue = match min == max {
            // No variance between the channels: monochromatic.
            // Hue doesn't matter, but it's common to set it to 0.
            true => 0.0,
            false => (match max {
                _r if max == self.red   => (self.green - self.blue) / diff + (if self.green < self.blue {6.0} else {0.0}),
                _g if max == self.green => (self.blue - self.red) / diff + 2.0,
                _b if max == self.blue  => (self.red - self.green) / diff + 4.0,
                _ => unreachable!("One channel is always max"),
            }) / 6.0,
        };

        HSV::new(
            hue * 360.0,
            saturation,
            max,
        )
    }
}
