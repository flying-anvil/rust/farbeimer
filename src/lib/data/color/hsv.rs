use crate::conversion::color_mode::{ToRgb, ToRgb8, ToHsv};

use super::{RGB, RGB8, Color};

/// Hue, Saturation, Value
/// In range of {
///   hue: 0..=360
///   saturation: 0..=1
///   value: 0..=1
/// }
#[derive(Debug, Clone)]
pub struct HSV {
    pub hue: f32,
    pub saturation: f32,
    pub value: f32,
}

impl HSV {
    pub fn new(hue: f32, saturation: f32, value: f32) -> Self {
        Self {
            hue,
            saturation,
            value,
        }
    }
}

impl Color for HSV {}

impl ToRgb for HSV {
    fn to_rgb(&self) -> RGB {
        // Shortcut if the saturation is 0 (< for failsafe).
        if self.saturation <= 0.0 {
            return RGB::new(self.value, self.value, self.value)
        }

        let reduced_hue = self.hue.rem_euclid(360.0) / 60.0;
        let quantized = reduced_hue as u8;
        let fraction = reduced_hue - (quantized as f32);

        let p = self.value * (1.0 - self.saturation);
        let q = self.value * (1.0 - (self.saturation * fraction));
        let t = self.value * (1.0 - (self.saturation * (1.0 - fraction)));

        match quantized {
            0 => RGB::new(self.value, t, p),
            1 => RGB::new(q, self.value, p),
            2 => RGB::new(p, self.value, t),
            3 => RGB::new(p, q, self.value),
            4 => RGB::new(t, p, self.value),
            5 => RGB::new(self.value, p, q),
            _ => unreachable!("Hue should be within [0, 360) at this point")
        }
    }
}

impl ToRgb8 for HSV {
    /// It's a chain conversion, as RGB::to_rgb8 only introduces a cannel-wise division, which is still necessary.
    fn to_rgb8(&self) -> RGB8 {
        self.to_rgb().to_rgb8()
    }
}

impl ToHsv for HSV {
    fn to_hsv(&self) -> HSV {
        self.clone()
    }
}
