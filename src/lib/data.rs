use self::color::Color;

pub mod color;

#[derive(Debug)]
pub struct Palette<T: Color> (Vec<T>);

impl<C: Color> Palette<C> {
    pub fn new(colors: Vec<C>) -> Palette<C> {
        Self(colors)
    }

    pub fn colors(&self) -> &[C] {
        &self.0
    }
}
