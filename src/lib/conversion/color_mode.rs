use crate::data::color::{RGB, RGB8, HSV, HSL};

pub trait ToRgb {
    fn to_rgb(&self) -> RGB;
}

pub trait ToRgb8 {
    fn to_rgb8(&self) -> RGB8;
}

pub trait ToHsv {
    fn to_hsv(&self) -> HSV;
}

pub trait ToHsl {
    fn to_(&self) -> HSL;
}

pub trait SuperConversion: ToRgb + ToRgb8 + ToHsv + ToHsv {}
