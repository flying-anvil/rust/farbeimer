use std::process::exit;

use clap::{Parser, Subcommand};

use self::command::GenerateColorPaletteCommand;

mod command;

#[derive(Parser, Debug)]
#[command(propagate_version = true)]
#[command(author, version, about, long_about = None)]
#[command(disable_colored_help = false)]
#[command(
    // help_template = "{author-with-newline} {about-section}Version: {version} \n {usage-heading} {usage} \n {all-args} {tab}"
    help_template = "{about}\n\n{usage-heading} {usage} \n\n{all-args}{tab}\n\n\x1B[1;4mAbout:\x1B[0m\n\x1B[3m  Authors: {author-with-newline}  Version: {version}\x1B[0m"
)]
/// Cli utility to generate color palettes
struct Cli {
    /// What to do
    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand, Clone, Debug)]
enum Command {
    #[command(name = "generate")]
    /// Generate a color palette
    GenerateColorPalette(GenerateColorPaletteCommand),
}

pub fn run() {
    let args = Cli::parse();

    #[cfg(debug_assertions)]
    println!("{args:#?}\n");

    let result = match args.command {
        Command::GenerateColorPalette(cmd) => cmd.run(),
    };

    if let Err(error) = result {
        eprintln!("{error}");
        exit(1);
    }
}
