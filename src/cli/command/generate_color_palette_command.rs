use std::error::Error;

use clap::Args;

#[derive(Args, Clone, Debug)]
pub struct GenerateColorPaletteCommand {
    /// How many colors to generate for one palette.
    #[arg(short='s', long="size")]
    size: Option<u16>,

    /// How many color palettes to generate.
    #[arg(short='c', long="count")]
    count: Option<u16>,
}

impl GenerateColorPaletteCommand {
    pub fn run(&self) -> Result<(), Box<dyn Error>> {
        todo!()
    }
}
